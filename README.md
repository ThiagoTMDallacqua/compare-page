## Compare Page Case

### Setup & Run

To quickly setup this project all you need to do is to run `npm install` or `yarn install` and, after that `npm start` or `yarn start`.

### Quick Overview

The application is a simple React application that uses Context API as an option to Redux.

When you hit the home page, the first thing that you'll see is a welcome card with a button to the actual compare page.

I choose that approach because in a real life application this kind of page wouldn't be the first thing that you usually see, so I took the oportunity to showcase one solution on how to do code spliting with `react-router`, which is something highly recommended.

I've used the `ProviderComponent` to store the products and the sorting method, which works kinda as a action/reducer. This way any component at any level of the application could use it, same as in a Redux application.

For this application I've used, mostly, CSS modules, except for the `ProgressBar` component, which shows the price comparison, and the `Checked` component, which is used to show that a product card has been clicked.

For these components I decided to use the `styled-components` as it is a faster and easier way to build components that are highly dinamic and to manipulate it easily with JavaScript.
