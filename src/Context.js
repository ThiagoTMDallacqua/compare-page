import React from 'react'
import { getProducts } from './helper'

export const { Provider, Consumer } = React.createContext()

export default class ProviderComponent extends React.Component {
  state = {
    products: []
  }

  sortByFastest = (a, b) => (
    a.lead_time > b.lead_time
      ? 1
      : -1
  )

  sortByCheapest = (a, b) => (
    a.cost > b.cost
      ? 1
      : -1
  )

  sortBy = sort => {
    if (sort === 'fastest') {
      this.setState({
        products: this.state.products.sort(this.sortByFastest)
      })
    } else {
      this.setState({
        products: this.state.products.sort(this.sortByCheapest)
      })
    }
  }

  componentDidMount() {
    this.setState({ products: getProducts() })
  }

  render() {
    const store = {
      products: this.state.products,
      sortBy: this.sortBy
    }
    return (
      <Provider value={store}>
        {this.props.children}
      </Provider>
    )
  }
}