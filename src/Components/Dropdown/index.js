import React from 'react'
import style from './styles.css'

const Dropdown = ({ options, onChange }) => {
  return (
    <select className={style.select} onChange={onChange} defaultValue={'initial'}>
      <option value="initial" disabled>
        Pick one option:
      </option>
      {options.map(({ value, label }) => (
        <option key={label} value={value}>
          {label}
        </option>
      ))}
    </select>
  )
}

export default Dropdown