import React from 'react'
import styled from 'styled-components'
import DHL from '../../assets/dhl.svg'
import DSV from '../../assets/dsv.svg'
import TNT from '../../assets/tnt.svg'
import UPS from '../../assets/ups.svg'
import style from './styles.css'

const getSelection = product => {
  const prd = product.split('_')[0]
  
  if (prd === 'dhl') {
    return <img className={style.icon} src={DHL} alt='dhl' />
  } else if (prd === 'dsv') {
    return <img className={style.icon} src={DSV} alt='dsv' />
  } else if (prd === 'tnt') {
    return <img className={style.icon} src={TNT} alt='tnt' />
  } else {
    return <img className={style.icon} src={UPS} alt='ups' />
  }
}

const compareLeadTime = (itemLead, totalLead) => (
  itemLead * 100 / totalLead
)

const getColor = (itemLead, totalLead) => {
  const percentage = compareLeadTime(itemLead, totalLead)

  if (percentage < 40) {
    return 'lightgreen'
  } else if (percentage < 70) {
    return 'burlywood'
  } else {
    return 'tomato'
  }
}

const ProgressBar = styled.div`
  width: calc(${({ lead, totalLead }) => compareLeadTime(lead, totalLead)}% - 6px);
  height: 5px;
  margin-left: 3px;
  border-radius: 20px;
  background-color: ${({ lead, totalLead }) => getColor(lead, totalLead)};
`

const Checked = styled.div`
  width: calc(100% - 2px);
  height: calc(100% - 2px);
  background-color: lightblue;
  border-radius: 20px;
  display: ${({showing}) => showing ? 'block' : 'none'}
`

class Card extends React.Component {
  state = {
    clicked: false
  }

  onClick = () => this.setState({ clicked: !this.state.clicked })

  render() {
    const { details, totalLeadTime } = this.props
    const company = details.product.replace(/_/g, ' ').toUpperCase()

    return (
      <div className={style.card} onClick={this.onClick}>
        <figure className={style.iconContainer}>
          {getSelection(details.product)}
        </figure>
        <div className={style.detailsBox}>
          <h3 className={style.title}>{company}</h3>
          <p className={style.price}>Price</p>
          <p className={style.details}>{Math.round(details.cost)} <span>{details.currency.toUpperCase()}</span></p>
          <div>
            <p className={style.leadTime}>Lead time: <span>{`${details.lead_time} Days`}</span></p>
            <div className={style.internalContainer}>
              <div className={style.progressBackground}>
                <ProgressBar lead={details.lead_time} totalLead={totalLeadTime} />
              </div>
              <div className={style.checkCircle}>
                <Checked showing={this.state.clicked} />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Card