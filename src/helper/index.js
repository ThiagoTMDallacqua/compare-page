import products from './products.json'

export function getProducts() {
  return products.items
}

export function getTotalLeadTime() {
  let total = 0

  products.items.forEach(({ lead_time }) => {
    if (lead_time > total) total = lead_time
  });

  return total
}