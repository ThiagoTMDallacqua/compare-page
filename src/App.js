import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import ProviderComponent from './Context'
import Loadable from 'react-loadable'
import HomePage from './Pages/Home'

const Loading = () => <div>Loading...</div>

const ComparePage = Loadable({
  loader: () => import('./Pages/Compare'),
  loading: Loading,
})

const App = () => {
  return (
    <ProviderComponent>
      <Router>
        <Switch>
          <Route exact path='/' component={HomePage} />
          <Route path='/compare' component={ComparePage} />
        </Switch>
      </Router>
    </ProviderComponent>
  )
}

export default App;
