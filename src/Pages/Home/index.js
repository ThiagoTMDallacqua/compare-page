import React from 'react'
import { Link } from 'react-router-dom'
import style from './styles.css'

const HomePage = () => (
  <main className={style.container}>
    <section className={style.card}>
      <h1 className={style.title}>This is the Home page, please click in the button bellow to go to the actual content.</h1>
      <Link className={style.link} to="/compare">Check out the actual content</Link>
    </section>
  </main>
)

export default HomePage