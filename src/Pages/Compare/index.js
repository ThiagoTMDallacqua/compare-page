import React from 'react'
import { Consumer } from '../../Context'
import { getTotalLeadTime } from '../../helper'
import Card from '../../Components/Card'
import Dropdown from '../../Components/Dropdown'
import style from './styles.css'

const ComparePage = () => (
  <div>
    <h2 className={style.filterHeading}>Sort your options</h2>
    <Consumer>
      {({ sortBy }) => (
        <Dropdown
          options={[
            {
              value: 'cheapest',
              label: 'Cheapest'
            },
            {
              value: 'fastest',
              label: 'Fastest'
            }
          ]}
          onChange={e => sortBy(e.target.value)}
        />
      )}
    </Consumer>
    <h2 className={style.filterHeading}>Available shipping options</h2>
    <div className={style.itemsContainer}>
      <Consumer>
        {({ products }) => (
          products.map(item => (
            <Card key={`${item.cost}${item.product}`} details={item} totalLeadTime={getTotalLeadTime()} />
          ))
        )}
      </Consumer>
    </div>
  </div>
)

export default ComparePage